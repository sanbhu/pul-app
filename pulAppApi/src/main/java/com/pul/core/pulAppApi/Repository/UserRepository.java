package com.pul.core.pulAppApi.Repository;

import com.pul.core.pulAppApi.Domain.Users.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<Users, Long> {
}
