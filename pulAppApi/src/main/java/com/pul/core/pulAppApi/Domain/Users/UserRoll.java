package com.pul.core.pulAppApi.Domain.Users;

public enum UserRoll {
   Admin, Primary, Secondary
}
