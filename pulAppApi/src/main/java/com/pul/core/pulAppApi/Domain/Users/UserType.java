package com.pul.core.pulAppApi.Domain.Users;

import javax.persistence.Entity;

public enum  UserType {
    Buyer, Seller, Both
}
