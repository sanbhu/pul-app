package com.pul.core.pulAppApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulAppApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PulAppApiApplication.class, args);
	}

}

