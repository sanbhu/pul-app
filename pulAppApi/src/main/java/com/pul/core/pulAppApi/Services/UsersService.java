package com.pul.core.pulAppApi.Services;

import com.pul.core.pulAppApi.Domain.Users.Users;

import java.util.List;

public interface UsersService {
    List<Users> getUsers();
    Users createUser(Users user);
    Users updateUser(Long userId, Users user);
    Users getUser(Long userId);
}
