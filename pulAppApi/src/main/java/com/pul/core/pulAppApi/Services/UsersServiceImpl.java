package com.pul.core.pulAppApi.Services;

import com.pul.core.pulAppApi.Domain.Users.Users;
import com.pul.core.pulAppApi.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UsersServiceImpl implements UsersService{

    private final UserRepository userRepository;

    @Autowired
    public UsersServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<Users> getUsers(){
        return StreamSupport.stream(userRepository.findAll().spliterator(), true)
                .collect(Collectors.toList());
    }

    @Override
    public Users createUser(Users user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public Users updateUser(Long userId, Users user) {
        return null;
    }

    @Override
    public Users getUser(Long userId) {
         return userRepository
                 .findById(userId)
                 .orElseGet(Users::new);
    }
}
