package com.pul.core.pulAppApi.Domain.Users;

import com.pul.core.pulAppApi.Domain.Common.Address;

import java.util.ArrayList;
import java.util.List;

public class Organization {
    private Address adress;
    private List<Users> users;

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<Users> users) {
        this.users = users;
    }

    public Address getAdress() {
        return adress;
    }

    public void setAdress(Address adress) {
        this.adress = adress;
    }
}
