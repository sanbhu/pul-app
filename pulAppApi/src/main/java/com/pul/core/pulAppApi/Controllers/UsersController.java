package com.pul.core.pulAppApi.Controllers;

import com.pul.core.pulAppApi.Domain.Users.Users;
import com.pul.core.pulAppApi.Domain.Users.UserRoll;
import com.pul.core.pulAppApi.Domain.Users.UserType;
import com.pul.core.pulAppApi.Services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final UsersService userService;

    @Autowired
    public UsersController(UsersService userService) {
        this.userService = userService;
    }

    @GetMapping("")
    public ResponseEntity<List<Users>> getUsers(){
        Users user1 = new Users();
        user1.setRoll(UserRoll.Admin);
        user1.setType(UserType.Buyer);
        user1.setFirstName("Sanjeev");
        user1.setLastName("Bhusal");

        userService.createUser(user1);

        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping()
    public ResponseEntity<Users> createUser(@RequestBody Users user, UriComponentsBuilder ucb){
        Users createdUser = userService.createUser(user);

        HttpHeaders headers = new HttpHeaders();

        URI locationUri =
                ucb.path("/users/")
                        .path(String.valueOf(createdUser.getId()))
                        .build()
                        .toUri();

        headers.setLocation(locationUri);
        ResponseEntity<Users> responseEntity =
                new ResponseEntity<>(
                        createdUser, headers, HttpStatus.CREATED);

        return responseEntity;
    }

    @GetMapping(path = "/{userId}", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public Users getUser(@PathVariable Long userId) {
        return userService.getUser(userId);
    }
}
